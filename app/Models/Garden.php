<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Garden extends Team
{
    use CrudTrait;

    /**
     * Scope a query to only include gardens of a given user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $userId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAllOfUser($query, $userId)
    {
        return $query->where('user_id', $userId)->with('zones');
    }

    /**
     * Scope a query to only include a single garden of a given user.
     * 
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $userId
     * @param  mixed  $gardenId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSingleOfUser($query, $userId, $gardenId)
    {
        return $query->where('user_id', $userId)->where('id', $gardenId)->with('zones');
    }

    /**
     * Get the zones for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function zones()
    {
        return $this->hasMany('App\Models\Zone', 'garden_id', 'id');
    }
}
