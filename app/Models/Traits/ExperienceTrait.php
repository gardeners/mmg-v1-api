<?php
namespace App\Models\Traits;

/**
 * This trait deals with all properties linked to experience gain
 * Model on which is applied the trait should have two attributes `experience` and `level`
 * TODO: Maybe define an interface for forcing the two attributes ?
 */

trait ExperienceTrait
{
    /**
     * Acceleration of experience cost per level
     * Used as a constant in the formulas for levelling up.
     */
    const ACC_EXPERIENCE_COST = 15;

    /**
     * Required experience to be level 2.
     * Users start at level 1.
     * Used as a constant in the formulas for levelling up.
     */
    const EXPERIENCE_LVL_2 = 10;

    /**
     * Number of things per level a user is expected to add.
     * Used as a constant in games.
     */
    const QUOTA_ADD_THINGS_PER_LEVEL = 3;


    /**
     * @param int $experience to give to this user.
     */
    public function addExperience($experience)
    {
        $this->experience += max(0, (int) $experience);
        // FIXME: Should this be here ?
        $this->level = $this->getLevel();
    }

    /**
     * @param int $experience
     */
    public function setExperience($experience)
    {
        $this->experience = max(0, (int) $experience);


    }

    /**
     * @return int the total amount of experience points this user has.
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Experience points acquired towards next level.
     * This is the total of experience points minus the experience points
     * required to attain the current level of the user.
     *
     * @return int
     */
    public function getExperienceProgress()
    {
        return $this->experience - self::experienceOf($this->getLevel());
    }

    /**
     * @return int the amount of manure points missing to gain next level.
     */
    public function getExperienceMissing()
    {
        return self::experienceOf($this->getLevel() + 1) - $this->experience;
    }

    /**
     * @return int the current level of this User.
     */
    public function getLevel()
    {
        return self::levelOf($this->experience);
    }

    /**
     * Should only be used when testing, as leveling up for users is
     * automatically done when adding experience with `addExperience`.
     *
     * @param int $level at which we want this user to be.
     */
    public function setLevel($level)
    {
        $this->setExperience(self::experienceOf($level));
    }

    /**
     * @thanks @Goutte for the formula.
     *
     * @param  int $experience
     * @return int the level attained with $experience points.
     */
    static function levelOf($experience)
    {
        $a = self::ACC_EXPERIENCE_COST;
        $d = self::EXPERIENCE_LVL_2;
        $n = floor(
            (3 * $a - 2 * $d + sqrt(pow(2 * $d - $a, 2) + 8 * $a * $experience))
            /
            (2 * $a)
        );

        return max($n, 1);
    }

    /**
     * @thanks @Goutte for the formula.
     * For a=15 and d=10 result is [0, 10, 30, 75, 135, 200 ...]
     * @param  int $level
     * @return int the experience required to be $level.
     */
    static function experienceOf($level)
    {
        $a = self::ACC_EXPERIENCE_COST;
        $d = self::EXPERIENCE_LVL_2;
        $n = $level;

        return ($d - $a) * ($n - 1) + $a * ($n * $n - $n) / 2;
    }

}