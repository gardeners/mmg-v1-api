<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantDefinitionWidenesses extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'plant_definition_widenesses';
    
    protected $fillable = [
        'min_width',
        'max_width',
    ];

    public static function countDistinct($column)
    {
        return self::distinct()->count($column);
    }

    public function plantDefinition()
    {
        return $this->hasMany(PlantDefinition::class, 'wideness_id', 'id');
    }
}
