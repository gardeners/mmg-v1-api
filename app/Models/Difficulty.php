<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Difficulty extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $fillable = [
        'name',
        'level'
    ];

    public static function maxLevel()
    {
        return self::max('level');
    }

    public static function countDistinct($column)
    {
        return self::distinct()->count($column);
    }
    
    public function plant_definition()
    {
        return $this->hasMany('App\Models\PlantDefinition', 'difficulty_id', 'id');
    }
}
