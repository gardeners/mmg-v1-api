<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $table = "weathers";

    public function openweathermapcode()
    {
        return $this->belongsTo('App\Models\OpenWeatherMapCode', 'open_weather_map_id');
    }
}