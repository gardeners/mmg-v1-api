<?php
namespace App\Models\Repositories;

use App\Models\Saying;

class SayingRepository extends BaseRepository
{
    public function __construct(Saying $model)
    {
        parent::__construct($model);
    }

    /**
     * Get a saying depending on the given date. 
     * If several sayings can be displayed, 
     * The ponderation is calculated on the frequency of the saying
     * lower is the frequency, higher is the probability to be returned.
     * https://framagit.org/gardeners/pangaia-server/-/wikis/models/Sayings#priorit%C3%A9
     * 
     * returns one randomly.
     */

    private function priorizeByFrequency($records)
    {
        /*
        Certains adages peuvent être dispensés tout au long de l'année. D'autres sur une période spécifique.
        Voire même un seul jour. Ainsi une priorité est donnée en fonction de leur durée.
        Prenons le contexte suivant:

        Dicton 0 : Toute l'année
        Dicton 1 : Du 1 au 13 janvier
        Dicton 2 : Le 5 janvier
        Dicton 3 : Du 12 au 24 janvier

        Donnera le résultat suivant:

        Le 3  jan 2024 le dicton est : Dicton 1 (99%)  ou Dicton 0 (1%)
        Le 5  jan 2024 le dicton est : Dicton 2 (91%) ou Dicton 1 (8%) ou Dicton 0 (1%) ( cas le plus compliqué TODO: trouver le bon algo)
        Le 13 jan 2024 le dicton est : Dicton 1 ou 3 (50/50) ou Dicton 0 (1%)
        Le 24 jan 2024 le dicton est : Dicton 1 (99%) ou Dicton 0 (1%)
        Le 31 jan 2024 le dicton est : Dicton 0

        Tout dicton pouvant apparaitre a, au minimum, 1% de chance d'apparaître.

        La logique implémentée ici : 
            Récupération de toutes les possibilités 
            Définition d'une valeur total_days => somme(date_end - date_beg) du résultat précédent
            Pourcentage calculé pour chaque ligne => total_days - (date_end - date_beg) / total_days
        */
        $result = null; // default if no records

        $total_days = 0;
        foreach ($records as $record) {

            $total_days += $record->days;
        }
        $random_number = random_int(0, 100);
        $range_min = $range_max = 0;
        foreach ($records as $record) {
            $range_max = $range_min + 100 * (($total_days - $record->days) / $total_days);
            if ($random_number >= $range_min && $random_number < $range_max) {
                $result = $record;
            }
            $range_min = $range_max;
        }

        return $result;
    }
    public function getByDate(string $d): Saying|null
    {
        // Dates are stored on EPOCH year in database.
        $pattern = '/\d{4}-(.*)/';
        $replacement = '1970-$1';
        $d = preg_replace($pattern, $replacement, $d);
        $d = $d . ' 00:00:00';
        $records = $this->model->select("title")->addSelect(\DB::raw("DATEDIFF(date_end, date_beg) AS days"))->whereDate('date_beg', "<=", $d)->whereDate("date_end", ">=", $d)->get();
        $record = $this->priorizeByFrequency($records);
        return count($records) > 0 ? $record : null;
    }
}
