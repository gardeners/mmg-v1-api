<?php

namespace App\Models\Repositories;

use App\Models\PlantDefinition;
use App\Models\Repositories\BaseRepository;

class PlantDefinitionRepository extends BaseRepository
{

  protected $model;

  public function __construct(PlantDefinition $model)
  {
    parent::__construct($model);
  }

  /**
   * Get all plant definitions join with difficulty order by difficulty level and picture table
   *
   * @return PlantDefinition models joined with Difficulty models
   */
  public function getAll()
  {
    $plantDefinitions = $this->model
      ->with('alternatePlantNames')
      ->join('difficulties', 'plant_definitions.difficulty_id', '=', 'difficulties.id')
      ->select('plant_definitions.*', 'difficulties.name as difficulty_name', 'difficulties.level')
      ->orderBy('difficulties.id', 'asc')
      ->get();

    return $plantDefinitions;
  }

  /**
   * Find a plant definition by id join with difficulty, requirements, widenesses, heights, picture table
   *
   * @param int $id
   * @return PlantDefinition|null
   */
  public function show($id)
  {
    $plantDefinition = $this->model
      ->with([
        'difficulty:id,name,level',
        'requirements:id,exposition_level,humidity_level,exposition_label,humidity_label',
        'widenesses:id,max_width,min_width',
        'heights:id,min_height,max_height',
        'plantFamily:id,name',
        'alternatePlantNames',
        'pictures'
      ])
      ->find($id);

    return $plantDefinition;
  }

  /**
   * Get the name of the plant definition by plant definition id
   */
  public function getNameByPlantDefinitionId(int $plantDefinitionId): ?string
  {
      $alternatePlantName = optional(PlantDefinition::find($plantDefinitionId))
          ->alternatePlantNames()
          ->wherePivot('is_most_common_plant_name', true)
          ->first();

      return optional($alternatePlantName)->name;
  }
}
