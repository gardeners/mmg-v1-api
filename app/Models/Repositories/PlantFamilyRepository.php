<?php

namespace App\Models\Repositories;

use App\Models\PlantFamily;

class PlantFamilyRepository extends BaseRepository
{
  public function __construct(PlantFamily $model)
  {
    parent::__construct($model);
  }

  public static function countDistinct($column)
  {
      return PlantFamily::distinct()->count($column);
  }
}