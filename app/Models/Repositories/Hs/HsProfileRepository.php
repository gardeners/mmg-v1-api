<?php
namespace App\Models\Repositories\Hs;

use App\Models\Hs\HsProfile;
use App\Models\Repositories\BaseRepository;

class HsProfileRepository extends BaseRepository
{
    public function __construct(HsProfile $model)
    {
        parent::__construct($model);
    }


    public function getByHsLevel($level)
    {
        return $this->model->where('level', $level)->get();
    }

}
