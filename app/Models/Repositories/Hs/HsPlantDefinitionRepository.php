<?php
namespace App\Models\Repositories\Hs;

use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsProfile;
use App\Models\Hs\HsProfilePlant;
use App\Models\Repositories\BaseRepository;
use Log;

class HsPlantDefinitionRepository extends BaseRepository
{

    public function __construct(HsPlantDefinition $model)
    {
        parent::__construct($model);
    }

    public function getByPlantDefinitionId($plantDefinitionId)
    {
        return $this->model
            ->where('plant_definitions_id', $plantDefinitionId)
            ->get();
    }

    public function getAllForProfile(HsProfile $profile)
    {
        // HS Profile has a level
        // Find plants that fit with the corresponding level
        // And that have not been found yet

        $alreadyFound = HsProfilePlant::where('hs_profile_id', '=', $profile->id)->select('hs_plant_definitions_id')->get()->toArray();

        $records = HsPlantDefinition::with(["plant_definition", "hs_plant_definition_indication"])
            ->where('level', $profile->level)->whereNotIn('id', $alreadyFound);
        return $records->get();
    }
}
