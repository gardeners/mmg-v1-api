<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use App\Traits\CustomHasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;


class User extends Authenticatable
{
    use CrudTrait;
    use HasApiTokens;
    use HasFactory;
    use CustomHasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use CrudTrait;
    use HasRoles;

    const LEVEL_NOOB = '1';
    const ROLE_DEMO = "Demo";
    const ROLE_USER = "User";
    const ROLE_BOTANIST = "Botanist";
    const ROLE_ADMIN = "Administrator";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'level',
    ];

    /**
     * Default value for attributes.
     */
    protected $attributes = array(
        'level' => User::LEVEL_NOOB
    );

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [
        'profile_photo_url',
        'role'
    ];

    /**
     * Get the role of the user.
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getRoleAttribute()
    {
        return $this->roles()->get();
    }

    /**
     * Check if the user is an admin
     */
    public function isAdmin()
    {
        return $this->hasRole(User::ROLE_ADMIN);
    }

    /**
     * Get the profile photo URL of the user.
     * 
     * @return string
     */
    public function getProfilePhotoUrlAttribute()
    {
        return $this->profilePhotoUrl();
    }

    /**
     * Get the user's plant definitions in the equipement
     */
    public function plantDefinitions()
    {
        return $this->belongsToMany(PlantDefinition::class, 'users_inventories', 'user_id', 'plant_definition_id')
            ->withPivot('quantity')
            ->withTimestamps();
    }
}