<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantDefinitonWidnesses extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'plant_definition_widnesses';
    
    protected $fillable = [
        'min_width',
        'max_width',
    ];

    public function plantDefinition()
    {
        return $this->belongsTo('App\Models\PlantDefinition', 'plant_definition_id', 'id');
    }
}
