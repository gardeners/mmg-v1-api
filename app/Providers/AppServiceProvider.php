<?php

namespace App\Providers;

use App\Policies\PlantDefinitionPolicy;
use App\Policies\PlantFamilyPolicy;
use App\Policies\SayingPolicy;
use App\Policies\TaskDefinitionPolicy;
use App\Policies\TaskDefinitionTypePolicy;
use App\Policies\ZonePolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Gate::define('create-plant-family', [PlantFamilyPolicy::class, 'create']);
        Gate::define('create-plant-definition', [PlantDefinitionPolicy::class, 'create']);
        Gate::define('create-saying', [SayingPolicy::class, 'create']);
        Gate::define('create-task-definition', [TaskDefinitionPolicy::class, 'create']);
        Gate::define('create-task-definition-type', [TaskDefinitionTypePolicy::class, 'create']);
        Gate::define('create-zone', [ZonePolicy::class, 'create']);
    }
}
