<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InventoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'exists:users,id'],
            'plant_definition_id' => ['required', 'integer', 'exists:plant_definitions,id'],
        ];
    }

    public function messages()
    {
        return [
            'user_id.exists' => 'The user could not be found.',
            'user_id.required' => 'The user is required.',
            'plant_definition_id.exists' => 'The plant definition could not be found.',
            'plant_definition_id.required' => 'The plant definition is required.',
        ];
    }
}
