<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="PictureResource",
 *     description="Picture definition resource",
 *     @OA\Xml(name="PictureResource"),
 *     
 *     @OA\Property(
 *       property="name",
 *       type="string",
 *       description="Name of the Picture."
 *     ),
 *     @OA\Property(
 *       property="is_main",
 *       type="boolean",
 *       description="Whether the Picture is the main one."
 *     )
 * )
 */
class PictureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->picture_name,
            'is_main' => $this->is_main,
        ];
    }
}
