<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     schema="InventoryItem",
 *     type="object",
 *     title="InventoryItem",
 *     description="InventoryItem definition resource",
 *     @OA\Xml(name="InventoryItem"),
 * 
 *     @OA\Property(
 *         property="id",
 *         type="integer"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string"
 *     ),
 *     @OA\Property(
 *         property="mainPicture",
 *         type="string"
 *     ),
 *     @OA\Property(
 *         property="quantity",
 *         type="integer"
 *     ),
 *     @OA\Property(
 *         property="updated_at",
 *         type="string",
 *         format="date-time"
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         type="string",
 *         format="date-time"
 *     )
 * )
 *
 * @OA\Schema(
 *     schema="InventoryResource",
 *     type="object",
 *     title="InventoryResource",
 *     description="InventoryItem definition resource",
 *     @OA\Xml(name="InventoryResource"),
 * 
 *     @OA\Property(
 *         property="user_id",
 *         type="integer"
 *     ),
 *     @OA\Property(
 *         property="inventory",
 *         type="array",
 *         @OA\Items(ref="#/components/schemas/InventoryItem")
 *     )
 * )
 */
class InventoryResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   * 
   */
  public function toArray($request)
  {
    return [
      'user_id' => $this->id,
      'inventory' => InventoryPlantDefinitionResource::collection($this->whenLoaded('plantDefinitions')),
    ];
  }
}
