<?php

namespace App\Http\Controllers;

use App\Http\Requests\HS\PlantFoundRequest;
use App\Models\Hs\HsLevel;
use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsProfile;
use App\Models\Hs\HsProfilePlant;
use App\Models\Repositories\Hs\HsPlantDefinitionRepository;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class HsController extends Controller
{
    public function __construct(HsPlantDefinitionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $profiles = HsProfile::where('user_id', Auth::id())->get();
        $levels = HsLevel::pluck("name", "id");
        return Inertia::render('Hs/GameHomepage', compact("profiles", "levels"));
    }
    public function startGame(HsProfile $profile)
    {
        /**
         * Launch a game for the corresponding profile
         * @var $profile : The profile selected by the user
         */
        $plants = $this->repository->getAllForProfile($profile);
        return Inertia::render('Hs/StartGame', ['plants' => $plants]);
    }

    public function requestForParentApproval()
    {
        /**
         * Approval step. The parent can access the given result and say if if fits or not
         */
        return Inertia::render('Hs/RequestForApproval');
    }
    public function debrief()
    {
        /**
         * Display a feedback on the plant properties. 
         */
        return Inertia::render('Hs/ApprovedAndDebrief');
    }

    public function approved(PlantFoundRequest $request)
    {
        $already_exists = HsProfilePlant::where([
            ["hs_profile_id", '=', $request->hs_profile_id],
            ["hs_plant_definitions_id", '=', $request->hs_plant_definition_id]
        ])->first();
        if (empty($already_exists)) {
            $plant = HsPlantDefinition::find($request->hs_plant_definition_id);
            $profile = HsProfile::find($request->hs_profile_id);

            // Memorize when we found it
            HsProfilePlant::create([
                "hs_profile_id" => $profile->id,
                "hs_plant_definitions_id" => $plant->id,
                "found_date" => date("Y-m-d")
            ]);

            // Increase profile experience
            $profile->addExperience($plant->level * HsProfile::ACC_EXPERIENCE_COST / HsProfile::QUOTA_ADD_THINGS_PER_LEVEL);
            $profile->save();
        }
    }

    public function reinit(HsProfile $profile)
    {
        return Inertia::render('Hs/StatisticsAndReinitialize', ['profile' => $profile]);
    }
}
