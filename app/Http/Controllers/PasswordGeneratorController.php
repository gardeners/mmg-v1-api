<?php

namespace App\Http\Controllers;

use App\Services\PasswordGeneratorService;
use Illuminate\Http\Request;

class PasswordGeneratorController extends Controller
{
    protected $passwordGeneratorService;

    public function __construct(PasswordGeneratorService $passwordGeneratorService)
    {
        $this->passwordGeneratorService = $passwordGeneratorService;
    }

    public function generate()
    {
        $password = $this->passwordGeneratorService->generateRandomPassword();
        return response()->json(['password' => $password]);
    }
}
