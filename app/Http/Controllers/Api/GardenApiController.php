<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GardenRequest;
use App\Http\Resources\GardenCollection;
use App\Http\Resources\GardenResource;
use App\Models\Garden;
use App\Models\Repositories\GardenRepository;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class GardenApiController extends Controller
{
  protected $gardenRepository;
  protected $user_id;

  public function __construct(GardenRepository $gardenRepository)
  {
    $this->gardenRepository = $gardenRepository;
  }

  /**
   *  @OA\Get(
   *    path="/api/v1/garden",
   *    operationId="getAllGardensByUser",
   *    tags={"Garden"},
   *    summary="Get all gardens of a user",
   *    description="Returns all user's gardens",
   *    security={{"bearerAuth": {}}},
   * 
   *    @OA\Response(
   *      response=200,
   *      description="Successful operation",
   *      @OA\JsonContent(
   *        type="array",
   *        @OA\Items(ref="#/components/schemas/GardenCollection")
   *      )
   *    ),
   *    @OA\Response(
   *      response=401,
   *      description="Unauthenticated"
   *    ),
   *    @OA\Response(
   *      response=403,
   *      description="Forbidden"
   *    ),
   *    @OA\Response(
   *      response=404,
   *      description="Resource Not Found"
   *    )
   *  )
   */
  public function index(GardenRequest $request)
  {
    $gardens = Garden::AllOfUser($request->userId());
    if (!Gate::authorize('viewAny', Garden::class)) {
      Log::debug("User " . Auth::id() . " is not allowed to show all gardens");
      abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
    }

    return new GardenCollection($gardens);
  }

  /**
   * @OA\Get(
   *    path="/api/v1/garden/{garden_id}",
   *    operationId="getSingleGardenByUser",
   *    tags={"Garden"},
   *    summary="Get a single garden for a user",
   *    description="Returns a single garden for a user",
   *    security={{"bearerAuth": {}}},
   * 
   *    @OA\Parameter(
   *      name="garden_id",
   *      in="path",
   *      description="ID of the garden",
   *      required=true,
   *      @OA\Schema(
   *        type="integer"
   *      ) 
   *    ),
   *    @OA\Response(
   *        response=200,
   *        description="Successful operation",
   *        @OA\JsonContent(
   *            type="array",
   *            @OA\Items(ref="#/components/schemas/GardenResource")
   *        )
   *    ),
   *    @OA\Response(
   *        response=401,
   *        description="Unauthenticated"
   *    ),
   *    @OA\Response(
   *        response=403,
   *        description="Forbidden"
   *    ),
   *    @OA\Response(
   *      response=404,
   *      description="Resource Not Found"
   *    )
   * )
   */
  public function show(string $id)
  {
    $garden = Garden::findOrFail($id);
    if (!Gate::authorize('view', $garden)) {
      Log::debug("User " . Auth::id() . " is not allowed to show garden $id");
      abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
    }
    Log::debug("Show garden $id");
    return (new GardenResource($garden))->response();
  }

  /**
   * @OA\Post(
   *   path="/api/v1/garden",
   *   operationId="createGarden",
   *   tags={"Garden"},
   *   summary="Create a garden",
   *   description="Create a garden for the user",
   *   security={{"bearerAuth": {}}},
   * 
   *   @OA\RequestBody(
   *     required=true,
   *     @OA\JsonContent(
   *       required={"name", "personal_team"},
   *       @OA\Property(property="name", type="string", example="My Garden"),
   *       @OA\Property(property="personal_team", type="boolean", example=true)
   *     )
   *   ),
   * 
   *   @OA\Response(
   *     response=201,
   *     description="Successful operation",
   *     @OA\JsonContent(ref="#/components/schemas/GardenCollection")
   *   ),
   *
   *   @OA\Response(
   *     response=401,
   *     description="Unauthenticated"
   *   ),
   *   @OA\Response(
   *     response=403,
   *     description="Forbidden"
   *   ),
   *   @OA\Response(
   *     response=404,
   *     description="Resource Not Found"
   *   )
   * )
   */
  public function store(GardenRequest $request)
  {

    $user = User::findOrFail($request->userId());
    $garden = $user->ownedTeams()->firstOrCreate([
      'user_id' => $request->userId(),
      'name' => $request['name'],
      'personal_team' => $request['personal_team'] ?? true,
    ]);

    return (new GardenResource($garden))
      ->response()
      ->setStatusCode(Response::HTTP_CREATED);
  }

  /**
   * @OA\Put(
   *   path="/api/v1/garden/{garden_id}",
   *   operationId="updateGarden",
   *   tags={"Garden"},
   *   summary="Update a garden",
   *   description="Update a garden for the user",
   *   security={{"bearerAuth": {}}},
   *
   *    @OA\Parameter(
   *      name="garden_id",
   *      in="path",
   *      description="ID of the garden",
   *      required=true,
   *      @OA\Schema(
   *        type="integer"
   *      )
   *    ),
   *
   *    @OA\RequestBody(
   *      required=true,
   *      @OA\JsonContent(
   *        required={"name", "personal_team"},
   *        @OA\Property(property="name", type="string", example="My Garden"),
   *        @OA\Property(property="personal_team", type="boolean", example=true)
   *      )
   *    ),
   * 
   *    @OA\Response(
   *      response=200,
   *      description="Successful operation",
   *      @OA\JsonContent(ref="#/components/schemas/GardenResource")
   *    ),
   * 
   *    @OA\Response(
   *      response=401,
   *      description="Unauthenticated"
   *    ),
   *    @OA\Response(
   *      response=403,
   *      description="Forbidden"
   *    ),
   *    @OA\Response(
   *      response=404,
   *      description="Resource Not Found"
   *    )
   * )
   */
  public function update(GardenRequest $request, string $id)
  {
    $garden = Garden::findOrFail($id);
    if (!Gate::authorize('update', $garden)) {
      Log::debug("User " . Auth::id() . " is not allowed to update garden $id");
      abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
    }
    Log::debug("Update garden $id");
    $garden->update($request->all());

    return (new GardenResource($garden))
      ->response()
      ->setStatusCode(Response::HTTP_ACCEPTED);
  }

  /**
   * @OA\Delete(
   *   path="/api/v1/garden/{garden_id}",
   *   operationId="deleteGarden",
   *   tags={"Garden"},
   *   summary="Delete a garden",
   *   description="Delete a garden for the user",
   *   security={{"bearerAuth": {}}},
   * 
   * 
   *   @OA\Parameter(
   *     name="garden_id",
   *     in="path",
   *     description="ID of the garden",
   *     required=true,
   *     @OA\Schema(
   *       type="integer"
   *     )
   *   ),
   * 
   *   @OA\Response(
   *     response=200,
   *     description="Successful operation",
   *     @OA\JsonContent(
   *       @OA\Property(property="message", type="string", example="Garden with id: 1 deleted successfully")
   *     )
   *   ),
   * 
   *   @OA\Response(
   *     response=401,
   *     description="Unauthenticated"
   *   ),
   *   @OA\Response(
   *     response=403,
   *     description="Forbidden"
   *   ),
   *   @OA\Response(
   *     response=404,
   *     description="Resource Not Found"
   *   )
   * )
   */
  public function destroy(string $id)
  {
    $garden = Garden::find($id);
    if (!Gate::authorize('delete', $garden)) {
      Log::debug("User " . Auth::id() . " is not allowed to delete garden $id");
      abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
    }
    $http_code = Response::HTTP_NOT_FOUND;
    if (!empty($garden)) {
      $garden->delete();
      $http_code = Response::HTTP_NO_CONTENT;
    }
    return response(null, $http_code);
  }
}
