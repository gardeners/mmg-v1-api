<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreSayingRequest;
use App\Http\Requests\Api\UpdateSayingRequest;
use App\Http\Resources\SayingResource;
use App\Http\Resources\SayingCollection;
use App\Models\Saying;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;


class SayingApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/saying",
     *      operationId="listSaying",
     *      tags={"Saying"},
     *      summary="List all sayings",
     *      description="Returns sayings data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        Log::debug("Listing the sayings");
        return new SayingCollection(Saying::paginate());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/saying",
     *      operationId="storeSaying",
     *      tags={"Saying"},
     *      summary="Store new saying",
     *      description="Returns saying data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"title"},
     *              @OA\Property(property="title", type="string", example="Jardiner avec la lune"),
     *              @OA\Property(property="date_beg", type="date", example="2023-05-23"),
     *              @OA\Property(property="date_end", type="date", example="2023-05-30"),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreSayingRequest $request)
    {
        if (!Gate::authorize('create-saying')) {
            Log::debug("User " . Auth::id() . " is not allowed to create saying");
            abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
        }
        $saying = Saying::create($request->all());
        return (new SayingResource($saying))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/saying/{id}",
     *      operationId="getSayingById",
     *      tags={"Saying"},
     *      summary="Get Saying information",
     *      description="Returns Saying data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Saying id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                      "id": 1,
     *                      "title": "Jardiner avec la lune", 
     *                      "date_beg": "2023-05-20",
     *                      "date_end": "2023-05-31",
     *                  }
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show saying $id");
        $saying = Saying::findOrFail($id);
        return (new SayingResource($saying))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/saying/{id}",
     *      operationId="updateSaying",
     *      tags={"Saying"},
     *      summary="Update existing saying",
     *      description="Returns updated saying data",
     *      @OA\Parameter(
     *          name="id",
     *          description="saying id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"title"},
     *              @OA\Property(property="title", type="string", example="Jardiner avec la lune"),
     *          ),
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdateSayingRequest $request)
    {
        Log::debug("Update saying $id");
        $saying = Saying::findOrFail($id);
        if (!Gate::authorize('update', $saying)) {
            Log::debug("User " . Auth::id() . " is not allowed to update saying");
            abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
        }
        $saying->update($request->all());
        return (new SayingResource($saying))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/saying/{id}",
     *      operationId="deleteSaying",
     *      tags={"Saying"},
     *      summary="Delete existing Saying",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="saying id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(string $id)
    {
        $saying = Saying::find($id);
        if (!Gate::authorize('delete', $saying)) {
            Log::debug("User " . Auth::id() . " is not allowed to delete saying");
            abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
        }
        $http_code = Response::HTTP_NOT_FOUND;
        if (!empty($saying)) {
            $saying->delete();
            $http_code = Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}
