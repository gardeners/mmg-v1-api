<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PlantDefinitionRequest;
use App\Http\Controllers\Admin\CrudController;
use App\Models\PlantDefinition;
use App\Models\Repositories\PlantDefinitionRepository;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PlantDefinitionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PlantDefinitionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        $this->permission_name = 'plant_definition';
        parent::setup();
        CRUD::setModel(\App\Models\PlantDefinition::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/plant-definition');
        CRUD::setEntityNameStrings('plant definition', 'plant definitions');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'name',
            'label' => 'Name',
            'type' => 'closure',
            'function' => function ($entry) {
                $plantDefinitionRepository = new PlantDefinitionRepository(new PlantDefinition());
                $name = $plantDefinitionRepository->getNameByPlantDefinitionId($entry->id);

                return $name;
            }
        ]);
        CRUD::column('family');
        CRUD::column('description');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        // FOR REPEATABLE FIELDS
        CRUD::setValidation(PlantDefinitionRequest::class);

        // CRUD::addField([
        //     'name' => 'alternate_names',
        //     'label' => 'Alternate Names',
        //     'type' => 'repeatable',
        //     'fields' => [
        //         [
        //             'name'    => 'name',
        //             'type'    => 'text',
        //             'label'   => 'Name',
        //         ],
        //         [
        //             'name'    => 'is_most_common_name',
        //             'type'    => 'checkbox',
        //             'label'   => 'Is Most Common Name',
        //         ],
        //     ],
        // ]);

        // FOR TABLE FIELDS
        // CRUD::addField([
        //     'name' => 'name',
        //     'label' => 'Name',
        //     'type' => 'text',
        // ]);

        // CRUD::addField([
        //     'name' => 'is_most_common_name',
        //     'label' => 'Is Most Common Name',
        //     'type' => 'checkbox',
        // ]);

        CRUD::field('name');

        CRUD::field('family');
        CRUD::field('description');
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}