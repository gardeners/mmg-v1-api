<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class PasswordGeneratorService
{
    public function generateRandomPasswordFromCharacters()
    {
        $length = 14;
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $password = '';

        $nb_letters = strlen($characters) / 2 - 1;
        $password .= $characters[random_int(0, $nb_letters)];
        $password .= $characters[random_int($nb_letters + 1, 2 * $nb_letters + 1)];

        for ($i = 0; $i < $length - 2; $i++) {
            $password .= $characters[random_int(0, $nb_letters)];
        }

        $password = str_shuffle($password);

        return $password;
    }

    public function generateRandomPasswordFromWords()
    {
        $jsonData = file_get_contents(base_path('resources/json/passwordList.json'));
        $lists = json_decode($jsonData, true);

        $numbers = $lists['numbers'];
        $animals = $lists['animals'];
        $colors = $lists['colors'];
        $verbs = $lists['verbs'];
        $adjectives = $lists['adjectives'];
        $conjunctions = $lists['conjunctions'];

        $number = $numbers[rand(0, count($numbers) - 1)];
        $animal = $animals[rand(0, count($animals) - 1)];
        $color = $colors[rand(0, count($colors) - 1)];
        $verb = $verbs[rand(0, count($verbs) - 1)];
        $adjective1 = $adjectives[rand(0, count($adjectives) - 1)];
        $conjunction = $conjunctions[rand(0, count($conjunctions) - 1)];
        $adjective2 = $adjectives[rand(0, count($adjectives) - 1)];

        while ($adjective1 === $adjective2) {
            $adjective2 = $adjectives[rand(0, count($adjectives) - 1)];
        }

        $password = $number . ' ' . $animal . ' ' . $color . ' ' . $verb . ' ' . $adjective1 . ' ' . $conjunction . ' ' . $adjective2;

        return $password;
    }

    public function generateRandomPassword()
    {
        $choice = random_int(0, 1);

        if ($choice == 0) {
            return $this->generateRandomPasswordFromCharacters();
        } else {
            return $this->generateRandomPasswordFromWords();
        }
    }
}

$service = new PasswordGeneratorService();
$service->generateRandomPassword();
