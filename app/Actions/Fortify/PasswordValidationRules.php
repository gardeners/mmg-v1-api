<?php

namespace App\Actions\Fortify;

use Illuminate\Contracts\Validation\Rule;

trait PasswordValidationRules
{
    /**
     * Get the validation rules used to validate passwords.
     *
     * @return array<int, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    protected function passwordRules(): array
    {
        return [
            'required',
            'string',
            'confirmed',
            function ($attribute, $value, $fail) {
                $wordCount = str_word_count($value);
                if ($wordCount >= 7) {
                    return;
                }

                if (strlen($value) >= 14 && preg_match('/[A-Z]/', $value) && preg_match('/[a-z]/', $value)) {
                    return;
                }

                $fail('Le mot de passe doit être une phrase composée de 7 mots ou avoir une longueur minimale de 14 caractères (contenant majuscules et minuscules).');
            },
        ];
    }
}