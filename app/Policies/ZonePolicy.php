<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Zone;

class ZonePolicy
{
    private function canAccess(User $user, Zone $zone): bool
    {
        return $user->belongsToTeam($zone->garden) || $user->hasRole([User::ROLE_ADMIN]);
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Zone $zone): bool
    {
        return $this->canAccess($user, $zone);
    }


    /**
     * Determine whether the user can create the model.
     */
    public function create(User $user, ): bool
    {
        return $user->hasRole([User::ROLE_USER, User::ROLE_BOTANIST,User::ROLE_ADMIN]);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Zone $zone): bool
    {
        return $this->canAccess($user, $zone);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Zone $zone): bool
    {
        return $this->canAccess($user, $zone);
    }
}
