<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InventoryPolicy
{
  use HandlesAuthorization;


  /**
   * Determine whether the user can view the inventory.
   *
   * @param  \App\Models\User  $authenticatedUser
   * @param  \App\Models\User  $user
   * @return mixed
   */
  public function access(User $authenticatedUser, User $user)
  {
      return $authenticatedUser->id === $user->id || $authenticatedUser->hasRole(User::ROLE_ADMIN);
  }
}
