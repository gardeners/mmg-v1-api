<?php

namespace App\Policies;

use App\Models\PlantFamily;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class PlantFamilyPolicy
{
    private function canAccess(User $user): bool
    {
        return $user->hasRole(User::ROLE_ADMIN);
    }
    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $this->canAccess($user);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, PlantFamily $plantFamily): bool
    {
        return $this->canAccess($user);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, PlantFamily $plantFamily): bool
    {
        return $this->canAccess($user);
    }
}
