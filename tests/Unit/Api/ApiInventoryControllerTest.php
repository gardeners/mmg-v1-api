<?php

namespace Tests\Unit\Api;

use App\Models\Inventory;
use App\Models\InventoryPlantDefinition;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;

class ApiInventoryControllerTest extends ApiTestCase
{
  protected string $route;
  protected $user;
  protected $model;
  protected array $mock;

  protected function setUp(): void
  {
    parent::setUp();
    $this->user = User::factory()->create();
    $this->route = "/api/v1/user/{$this->user->id}/inventory";
    $this->model = User::class;
    $this->mock = $this->model::factory()->definition();

    $this->attrChecklist_for_ApiResource = [
      "user_id",
      "inventory"
    ];
  }

  /**
   * Non connected users.
   */
  public function test_unauthenticated_user_should_have_a_401(): void
  {
    // A non connected user cannot use the Api.
    $response = $this->get($this->route);
    $response->assertStatus(Response::HTTP_UNAUTHORIZED);
  }
}
