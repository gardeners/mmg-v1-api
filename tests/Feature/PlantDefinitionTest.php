<?php

namespace Tests\Feature;

use App\Http\Requests\PlantDefinitionRequest;
use App\Models\Difficulty;
use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsPlantDefinitionIndication;
use App\Models\PlantDefinition;
use App\Models\PlantDefinitionRequirements;
use App\Models\TaskDefinition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PlantDefinitionTest extends TestCase
{
    use RefreshDatabase;

    protected $plantDefinition;
    protected $difficulty;

    protected function setUp(): void
    {
        parent::setUp();

        $this->difficulty = Difficulty::factory()->create();
        $this->plantDefinition = PlantDefinition::factory()->create(['difficulty_id' => $this->difficulty->id]);
    }

    public function test_can_create_plant_definition() : void
    {
        // Assert the plant_definition was created
        $this->assertDatabaseHas('plant_definitions', ['id' => $this->plantDefinition->id]);
    }

    public function test_can_read_plant_definition() : void
    {
        $foundPlantDefinition = PlantDefinition::find($this->plantDefinition->id);

        $this->assertNotNull($foundPlantDefinition);
    }

    public function test_can_update_plant_definition() : void
    {
        // Update the plant_definition
        $this->plantDefinition->description = 'Super description de plante';
        $this->plantDefinition->latin_name = 'Lorem Ipsum';
        $this->plantDefinition->save();

        // Assert the plant_definition was updated
        $this->assertDatabaseHas('plant_definitions', ['id' => $this->plantDefinition->id, 'description' => 'Super description de plante']);
        $this->assertDatabaseHas('plant_definitions', ['id' => $this->plantDefinition->id, 'latin_name' => 'Lorem Ipsum']);
    }

    public function test_can_delete_plant_definition() : void
    {
        // Delete the plant_definition
        $this->plantDefinition->delete();

        // Assert the plant_definition was deleted
        $this->assertDatabaseMissing('plant_definitions', ['id' => $this->plantDefinition->id]);
    }


    /**
     * Difficulty
     */
    public function test_can_create_plant_definition_difficulty() : void
    {
        // Assert the plant_definition_difficulty was created
        $this->assertEquals($this->difficulty->id, $this->plantDefinition->difficulty_id);
    }

    public function test_can_update_plant_definition_difficulty() : void
    {
        // Update the plant_definition_difficulty
        $newDifficulty = Difficulty::factory()->create();
        $this->plantDefinition->difficulty_id = $newDifficulty->id;
        $this->plantDefinition->save();

        // Assert the plant_definition_difficulty was updated
        $this->assertEquals($newDifficulty->id, $this->plantDefinition->difficulty_id);

        $newDifficulty->delete();
    }

    public function test_can_delete_plant_definition_difficulty() : void
    {
        // Delete the plant_definition, difficulty and newDifficulty
        $this->plantDefinition->delete();
        $this->difficulty->delete();

        // Assert the plant_definition and difficulty were deleted
        $this->assertDatabaseMissing('plant_definitions', ['id' => $this->plantDefinition->id]);
        $this->assertDatabaseMissing('difficulties', ['id' => $this->difficulty->id]);
    }

    /**
     * Plant Definition Requirements Relationship
     */
    public function test_plant_definition_requirements_relationship() : void
    {
        $plantDefinition = PlantDefinition::factory()->create();
        $plantDefinitionRequirements = PlantDefinitionRequirements::factory()->create();
        
        $plantDefinition->requirement_id = $plantDefinitionRequirements->id;
        $plantDefinition->save();
        
        $this->assertEquals($plantDefinitionRequirements->id, $plantDefinition->requirement_id);
        
        $plantDefinition->delete();
        $plantDefinitionRequirements->delete();
    }

    /**
     * Task Definitions Relationship
     */
    public function test_task_definitions_relationship() : void
    {
        $plantDefinition = PlantDefinition::factory()->create();
        $taskDefinition = TaskDefinition::factory()->create(['plant_definition_id' => $plantDefinition->id]);

        $this->assertTrue($plantDefinition->taskDefinitions->contains($taskDefinition));

        $plantDefinition->delete();
    }
}
