<?php

namespace Tests\Unit\Repositories\Hs;

use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsProfile;
use App\Models\Hs\HsProfilePlant;
use App\Models\Repositories\Hs\HsProfilePlantRepository;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsProfilePlantRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->seed(PermissionSeeder::class);
    }

    use RefreshDatabase;

    /** @test */
    public function it_can_retrieve_hs_profile_plants_by_hs_profile_id()
    {
        $hsProfile = HsProfile::factory()->create();
        $hsProfilePlants = HsProfilePlant::factory()->count(2)->create([
            'hs_profile_id' => $hsProfile->id,
        ]);

        $repository = new HsProfilePlantRepository(new HsProfilePlant());
        $fetchedProfilePlants = $repository->getByHsProfileId($hsProfile->id);

        $this->assertCount(2, $fetchedProfilePlants);
        $this->assertTrue($fetchedProfilePlants->first()->is($hsProfilePlants->first()));
    }

    /** @test */
    public function it_can_retrieve_hs_profile_plants_by_hs_plant_definition_id()
    {
        $hsPlantDefinition = HsPlantDefinition::factory()->create();
        $hsProfilePlants = HsProfilePlant::factory()->count(2)->create([
            'hs_plant_definitions_id' => $hsPlantDefinition->id,
        ]);

        $repository = new HsProfilePlantRepository(new HsProfilePlant());
        $fetchedPlantDefinitionPlants = $repository->getByHsPlantDefinitionId($hsPlantDefinition->id);

        $this->assertCount(2, $fetchedPlantDefinitionPlants);
        $this->assertTrue($fetchedPlantDefinitionPlants->first()->is($hsProfilePlants->first()));
    }

}
