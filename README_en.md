<a href="https://laravel.com">
  <img src="https://img.shields.io/badge/%20%20-Laravel%20-grey?logo=laravel" alt="Laravel Badge" width="145"/>
</a>
<a href="https://www.docker.com/">
  <img src="https://img.shields.io/badge/%20%20-Docker%20-grey?logo=docker" alt="Docker Badge" width="140"/>
</a>
<a href="https://vuejs.org/">
  <img src="https://img.shields.io/badge/%20%20-VueJS%20-grey?logo=vue.js" alt="VueJS Badge" width="127"/>
</a>

# Project Overview

***Pangaia*** is a web and mobile application that helps and assists gardeners in planning their tasks.

# Dependencies
This project requires:
 - **PHP** *8.1* with extensions:
   - *PHP-Mbstring*
   - *PHP-XML*
   - *PHP-Curl*
   - *PHP-Zip*
   - *PHP-FileInfo*
 - **Composer** *2.5*
 - **Docker** *20.10*
 - **NPM** *9.6*
 - **Docker-compose** *1.29*
 - **NodeJS** *20.2*

# Installation
## SSH 
```shell
git clone git@framagit.org:gardeners/pangaia-server.git 
```
```shell
cd pangaia-server
```
```shell
make install
```
## HTTP
```shell
git clone https://framagit.org/gardeners/pangaia-server.git 
```
```shell
cd pangaia-server
```
```shell
make install
```

# Starting the Project
## In development mode 
```shell
make dev
```
## In production mode
```shell
make build
```

# Project Structure
## Docker Containers  
- **laravel.test** : Our project
- **mysql** : Database
- **mailpit** : SMTP server for emails
- **caddy** : HTTPS server 

## Laravel
- **laravel/fortify** : Provides backend controllers and scaffolding for Laravel authentication.
- **laravel/framework** : The main Laravel framework.
- **laravel/jetstream** : Provides a basic structure for Laravel using the Tailwind CSS framework.
- **laravel/sail** : Provides Docker files for running a basic Laravel application.
- **laravel/sanctum** : Offers a lightweight authentication system for single-page applications (SPAs) and simple APIs.
- **laravel/tinker** : A powerful REPL (Read-Eval-Print Loop) for the Laravel framework.

## API
- **DarkaOnLine/L5-Swagger** : Swagger automatically generates Swagger/OpenAPI documentation files from our code comments. Swagger offers an interactive user interface. 

## Routes
 - API GET + token: http://pangaia.local/api/user
 - Back office : http://pangaia.local/admin/dashboard
 - API documentation : http://pangaia.local/api/documentation

To view all the project routes:
```shell
php artisan route:list
```

# License

[![Hippocratic License HL3-CORE](https://img.shields.io/static/v1?label=Hippocratic%20License&message=HL3-CORE&labelColor=5e2751&color=bc8c3d)](https://firstdonoharm.dev/version/3/0/core.html)
