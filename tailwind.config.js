const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
                mainFont: ['Tw Cen MT', 'sans-serif'],
                secondaryFont: ['Poppins', 'sans-serif'],
                inter: ['Inter', 'sans-serif'],
            },
            fontSize: {
                'xxs': '0.625rem',
                'xxxs': '0.5rem',
            },
            height: {
                'svh-90': '90svh',
                'svh-85': '85svh',
                'svh-80': '80svh',
                'svh-75': '75svh',
                'svh-50': '50svh',
                'svh-60': '60svh',
                'svh-40': '40svh',
                'svh-35': '35svh',
                'svh-30': '30svh',
            },
            colors: {
                'main-color': '#002a42',
                'main-white': '#f3f4f6',
                'main-black': '#111111',
                'main-grey': '#dddddd',
                'dark-grey': '#a6a2a2',
                'main-red': '#F76262',
                'second-color': '#95b8d1',
                'second-80': '#aac6da',
                'second-60': '#bfd4e3',
                'second-40': '#d5e3ed',
                'second-selection': '#105A90',
                'third-color': '#c0e9ff',
                'third-80': '#cdedff',
                'third-60': '#d9f2ff',
                'third-40': '#ebf8ff',
                'fourth-color': '#b5e2fa',
                'fourth-80': '#c4e8fb',
                'fourth-60': '#d3eefc',
                'fourth-40': '#e1f3fd',
                'particule-second': '#EDDEA4',
                'fog-color': 'rgba(91, 118, 135, 0.5)',

                // MDS Modif
                'main-orange': '#FF6714',
                'main-orange-10': '#FFE6D9',
                'main-green': '#038340',
                'light-green': '#FFECC4',
                'mustard': '#FAA429',
            },
            backgroundImage: (theme) => ({
                'linear-weather' : 'linear-gradient(90deg, #D5E3ED 0%, #95B8D1 100%)',
                'linear-background' : 'linear-gradient(180deg, rgba(255,236,196,1) 0%, rgba(255,247,230,1) 90%, rgba(255,255,255,1) 100%)',
                'linear-menu' : 'linear-gradient(180deg, rgba(255,237,228,1) 0%, rgba(255,246,242,1) 90%, rgba(255,255,255,1) 100%)',
                'linear-loader': 'url("/GraphicRessources/Images/linear-loader.svg")',
                'linear-black': `linear-gradient(180deg, ${theme('colors.main-black')} 0%, transparent 100%)`,
                'zone-background': 'url("/GraphicRessources/Images/zone-background.svg")',
            }),
            boxShadow: {
                'main-button': 'inset -5px -6px 12px rgba(0, 0, 0, 0.25), inset 4px 4px 10px rgba(255, 255, 255, 0.41), 2px 4px 11px rgba(0, 0, 0, 0.15)',
                'light-zone': '1px 1px 1px rgba(0, 0, 0, 0.2), inset 0px 4px 4px rgba(255, 255, 255, 0.25)',
                'goback-button': 'inset -1px -1px 4px rgba(58, 125, 173, 0.2), inset 2px 2px 6px rgba(255, 255, 255, 0.4)',
                'secondary-button': '0px 4px 10px #95b8d1',
                'checkbox-button': '1px 2px 10px rgba(0, 0, 0, 0.15)',
                'container': '2px 4px 10px rgba(0, 0, 0, 0.1)',
                'search-input': '1px 2px 10px rgba(0, 0, 0, 0.1)',
                'card-layout': '4px 4px 8px rgba(0, 0, 0, 0.08)',
                'progress-bar-login': 'inset -2px -2px 6px rgba(58, 125, 173, 0.2), inset 2px 4px 6px rgba(255, 255, 255, 0.4)',
            },
            rotate: {
                '360': '360deg',
            },
        },
    },

    plugins: [
        require('@tailwindcss/forms'), 
        require('@tailwindcss/typography'),
        function ({ addBase, theme }) { 
            addBase({
                'h1': { 
                    fontFamily: theme('fontFamily.mainFont'), 
                    fontWeight: '600', 
                    fontSize: '45px', 
                    lineHeight: '49px' 
                },
                'h2': {
                    fontFamily: theme('fontFamily.mainFont'),
                    fontWeight: '600',
                    fontSize: '30px',
                    lineHeight: '34px',
                },
                'h3': {
                    fontFamily: theme('fontFamily.mainFont'),
                    fontWeight: '500',
                    fontSize: '16px',
                    lineHeight: '29px',
                },
                'h4': {
                    fontFamily: theme('fontFamily.mainFont'),
                    fontWeight: '600',
                    fontSize: '14px',
                    lineHeight: '17px',
                },
            })
        }
    ],
};
