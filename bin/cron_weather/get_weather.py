#!./venv/bin/python

"""
Script for calling a weather api. 
Meant to be automated by a cron task. 
For example, if you which to automate this task every hour at ??h00
Eg : 0 * * * * cd /home/docus/Documents/projets/pangaia/pangaia-server/bin/cron_weather/ && ./get_weather.py
This script is also automatically run while launching make dev

"""

from dotenv import load_dotenv
import os
import mariadb
import sys
import requests
from datetime import datetime

def k_to_c(temp:int):
    """
    Convert Kelvin temperature to Celcius temperature.
    :param temp:
    :return:
    """
    return round(temp - 273.15, 2)

def request_api(zip_code, country_code : str = 'FR'):
    """ Call the Open Weather map API and return data in expected format. """
    api_link = f"http://api.openweathermap.org/data/2.5/weather?zip={zip_code},{country_code}&appid={api_key}"
    # Getting the request from OpenWeatherMap API and formatting it to Dict/JSON format

    reponse = requests.get(api_link, timeout=20)
    api_content_json = reponse.json()
    zip_code_data = {
        "code": zip_code,
        "name": api_content_json["name"],
        "longitude": round(api_content_json.get('coord').get('lon'), 2),
        "latitude": round(api_content_json.get('coord').get('lat'), 2),
        "sea_level": api_content_json["main"]["sea_level"],
        "grnd_level": api_content_json["main"]["grnd_level"]
    }
    weather_data = {
        "code": zip_code,
        "pressure": api_content_json["main"]["pressure"],
        "humidity": api_content_json["main"]["humidity"],
        "temp": k_to_c(api_content_json["main"]["temp"] ),
        "open_weather_map_id": api_content_json["weather"][0]["id"],
        "feels_like" : k_to_c(api_content_json["main"]["feels_like"] ),
        "wind_speed":api_content_json["wind"]["speed"],
        "wind_deg" : api_content_json["wind"]["deg"]
    }

    return zip_code_data, weather_data

def get_current_weather(conn):
    """
    Parse the database for checking which zipcodes to get.
    Call the request_api method
    Store result in the database.
    """
    cur = conn.cursor()

    # Which  zipcodes to get
    cur.execute("SELECT id, code, name from zip_codes;")
    zipcodes = cur.fetchall()

    # Query for updating zipcode related data if needed
    zipcode_sql = "UPDATE zip_codes SET name = ?, longitude = ?, latitude = ?, sea_level = ?, grnd_level = ? WHERE code = ?"

    # Query for the weather data we get
    weather_sql = "INSERT INTO weathers(zip_code_id, open_weather_map_id, temp, feels_like, pressure, humidity, wind_speed, wind_deg, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?)"


    for (id, code, name) in zipcodes:
        # Call Api and get a cleaned output
        zc, w = request_api(code)
        if name is None:
            """ First time we are extracting data concerning this zip code. Let's backup some additionnal data. """
            values = (zc['name'], zc['longitude'], zc['latitude'], zc['sea_level'], zc['grnd_level'], zc['code'])
            cur.execute(zipcode_sql, values)

        # Save weather data
        weather_values = (id, w['open_weather_map_id'], w['temp'], w['feels_like'], w['pressure'], w['humidity'], w[
            'wind_speed'], w['wind_deg'], datetime.now(), datetime.now())
        cur.execute(weather_sql, weather_values)

    conn.commit()
    conn.close()

# Import the API key from the .env file
load_dotenv()
api_key = os.getenv("OPENWEATHERMAP_KEY")
db_port = os.getenv("DB_PORT")
db_database = os.getenv("DB_DATABASE")
db_user = os.getenv("DB_USERNAME")
db_pass = os.getenv("DB_PASSWORD")

if api_key is not None and len(api_key) > 0 :
    # Connect to MariaDB Platform
    try:
        conn = mariadb.connect(
            user=db_user,
            password=db_pass,
            host="127.0.0.1",
            port= int(db_port),
            database=db_database

        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)

    get_current_weather(conn)
else:
    print("No api_key set so no weather generated ! Please set a value for  OPENWEATHERMAP_KEY")
