
echo "Stopping MySQL..." && (systemctl is-active --quiet mysql && sudo systemctl stop mysql && echo "\033[1;32m✔MySQL stopped\033[0m") || echo "\033[1;32m✔MySQL was already stopped\033[0m"
echo "Stopping Apache..." && (systemctl is-active --quiet apache2 && sudo systemctl stop apache2 && echo "\033[1;32m✔Apache stopped\033[0m") || echo "\033[1;32m✔Apache was already stopped\033[0m"

