import { defineStore } from 'pinia'

export const useRegisterStore = defineStore('useRegisterStore', {
  state: () => ({
    step: 1,
    maxSteps: 4,
  }),
  actions: {
    incrementStep() {
      this.step++;
    },
    decrementStep() {
      if (this.step > 1) this.step--;
    },
    setStep(value) {
      this.step = value;
    },
    resetStep() {
      this.step = 1;
    },
  },
})