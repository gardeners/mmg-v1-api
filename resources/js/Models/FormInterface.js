/**
 * Abstract Class FormInterface.
 *
 * @class FormInterface
 */
export class FormInterface {

    constructor() {
      if (this.constructor == FormInterface) {
        throw new Error("Abstract classes can't be instantiated.");
      }
    }
  
    getForm() {
        throw new Error("Method 'getForm()' must be implemented.");
    }
}