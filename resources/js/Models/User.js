import {FormInterface} from './FormInterface.js';
import {roles} from './Role.js'
import {levels} from './Level.js'

export class User extends FormInterface{
  getForm() {
    return {
      canCreateTeams: true,
      name: "",
      email: "",
      username: "",
      age: null,
      zipcode:"",
      password: "",
      password_confirmation: "",
      terms: false,
      role: roles.user,
      level: levels.noob.level,
    }
  }

}  
