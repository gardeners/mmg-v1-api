import { ref } from 'vue';

export const showPassword = ref(false);

export const togglePasswordVisibility = () => {
  showPassword.value = !showPassword.value;
}

export const generatePassword = async (form) => {
    try {
      const response = await fetch('/generate-password');
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      form.password = data.password;
      showPassword.value = true;
    } catch (error) {
      console.error('Error fetching password:', error);
    }
  }