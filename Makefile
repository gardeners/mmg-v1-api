APP_NAME=pangaia-app


# Executables (local)
DOCKER = docker
DOCKER_COMP = docker compose
DOCKER_COMP_DEV = $(DOCKER_COMP) -f docker-compose.dev.yml
# Docker containers
PHP_CONT = $(DOCKER_COMP_DEV) exec $(APP_NAME)
PHP_CONT1 = $(DOCKER) exec $(APP_NAME)

COMPOSER=$(PHP_CONT) composer
ARTISAN=$(PHP_CONT1) php artisan
NPM     = $(PHP_CONT1) npm

USER=$$(whoami)
DOCKER_NETWORK = sail

ifeq ($(OS),Windows_NT)
	detected_OS=Windows
else
	detected_OS=$(shell uname -s)
endif

# Misc
.DEFAULT_GOAL: help
.PHONY : help build up start down logs sh composer vendor artisan

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker ——————————————————————————————————————————————————————————————
install: ## Install the application
# Check if .env file is missing
ifeq ($(wildcard .env),)
	@echo "\033[1;31m✖ Veuillez copier le fichier .env.sample en .env et l'adapter à vos besoins\033[0m"
endif

# Check if user is a docker user
ifneq ($(findstring docker,$(shell groups)), docker)
	@echo "\033[1;31m✖ User launching the Makefile must have docker group\033[0m"
	exit 1
endif
	docker network inspect $(DOCKER_NETWORK) > /dev/null 2>&1 || (echo "Creating docker network" && docker network create $(DOCKER_NETWORK))
	@echo "Composer install" && $(DOCKER_COMP) -f docker-preinstall.yml up
	@echo "Starting container..." && $(DOCKER_COMP) up -d && echo "\033[1;32m✔ Launched mailpit and database\033[0m"
	@echo "Starting container..." && $(DOCKER_COMP_DEV) up -d && echo "\033[1;32m✔Succeeded\033[0m"
	@echo "Running Laravel migrations..." && $(ARTISAN) migrate --seed && echo "\033[1;32m✔Laravel migrations run successfully.\033[0m" || echo "\033[1;31m✖Failed to run Laravel migrations\033[0m"
	@echo "Running BackPack..." && ($(ARTISAN) backpack:install && echo "\033[1;32m✔BackPack migrations run successfully.\033[0m") || echo "\033[1;31m✖Failed to run BackPack migrations\033[0m"
	@echo "Generating artisan key " && $(ARTISAN) key:generate
	@echo "Generating a storage link for image upload" && $(ARTISAN) storage:link
	@echo "npm install" && $(NPM) install
	@echo "npm build " && $(NPM) run build
	@echo "Install python requirements" && python -m venv bin/cron_weather/venv && ./bin/cron_weather/venv/bin/pip install -r ./bin/cron_weather/requirements.txt

up: ## Launch the docker services
ifeq ($(detected_OS),Windows)
	cmd /C bin/stop_services.bat
else ifeq ($(detected_OS),Linux)
	sh bin/stop_services.sh
endif
	@echo Starting Docker containers... && $(DOCKER_COMP) up -d
	@echo Starting Docker dev containers... && $(DOCKER_COMP_DEV) up -d

down: ## Shuts down the docker services
	@echo Starting Docker containers... && $(DOCKER_COMP) down
	@echo Stopping Docker containers... && $(DOCKER_COMP_DEV) down && echo "\033[1;32m✔Succeeded\033[0m"

dev: ## Launch the docker services and builds node for development
ifeq ($(detected_OS),Windows)
	cmd /C bin/stop_services.bat
else ifeq ($(detected_OS),Linux)
	sh bin/stop_services.sh
endif
	@echo Starting Docker containers... && $(DOCKER_COMP) up -d --remove-orphans
	@echo Starting Docker dev containers... && $(DOCKER_COMP_DEV) up -d && echo "\033[1;32m✔Docker startup succeeded\033[0m"
	@echo "Waiting for database to be up" && sleep 2
	@echo Update weather && ./bin/cron_weather/venv/bin/python ./bin/cron_weather/get_weather.py
	@echo Compiling assets... && $(NPM) run dev || echo "\033[1;31m✖Failed to compile\033[0m"
	

build: ## Launch the docker services and build the application for prod
	@echo Starting Docker containers... && $(DOCKER_COMP) up -d &&echo "\033[1;32m✔Docker startup succeeded\033[0m"
	@echo "Running Laravel migrations..." && $(ARTISAN) migrate && echo "\033[1;32m✔Laravel migrations run successfully.\033[0m" || echo "\033[1;31m✖Failed to run Laravel migrations\033[0m"
	@echo Compiling assets... && $(NPM) run build || echo "\033[1;31m✖Failed to compile\033[0m"

## —— Production ——————————————————————————————————————————————————————————————
prod: ## Run containers for production environment
	@echo Starting containers... && $(DOCKER_COMP) -f docker-compose.prod.yml up --build -d

update:
	@echo Stopping Docker containers... && $(DOCKER_COMP) -f docker-compose.prod.yml down pangaia.app 
	@echo Restarting Docker containers... && $(DOCKER_COMP) -f docker-compose.prod.yml up --build -d pangaia.app

## —— Artisan/Laravel ——————————————————————————————————————————————————————————————
artisan: ## Run artisan, pass the parameter "c=" to run a given command, example: make artisan c='route:list'
	@$(eval c ?=)
	@$(ARTISAN) $(c)

## —— Node/npm ——————————————————————————————————————————————————————————————
npm: ## Run npm, pass the parameter "c=" to run a given command, example: make npm c='run watch'
	@$(eval c ?=)
	@$(NPM) $(c)

## —— Swagger ——————————————————————————————————————————————————————————————
swagger: ## Build the swagger pages
	$(ARTISAN) l5-swagger:generate --all

clear_cache: ## Cleanup all possible caches.
	@echo Clearing Laravel cache... && $(ARTISAN) cache:clear && echo Cache cleared successfully! || (echo Failed to clear cache. && exit 1)
	@echo Clearing view cache... && $(ARTISAN) view:clear && echo View cache cleared successfully! || (echo Failed to clear view cache. && exit 1)
	@echo Clearing config cache... && $(ARTISAN) config:clear && echo Config cache cleared successfully! || (echo Failed to clear config cache. && exit 1)
	@echo Clearing route cache... && $(ARTISAN) route:clear && echo Route cache cleared successfully! || (echo Failed to clear route cache. && exit 1)

## —— Unit tests ——————————————————————————————————————————————————————————————
test: ## Run unit tests
	$(ARTISAN) test

test_os:
ifeq ($(detected_OS),Windows)
	@echo WINDOWS
else ifeq ($(detected_OS),Linux)
	@echo LINUX
else
	@echo "$(detected_OS)"
endif

log:
	@tail -f storage/logs/laravel.log
