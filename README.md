# PANGAIA
[![Hippocratic License HL3-CORE](https://img.shields.io/static/v1?label=Hippocratic%20License&message=HL3-CORE&labelColor=5e2751&color=bc8c3d)](https://firstdonoharm.dev/version/3/0/core.html)

-   <a href="./README_fr.md"> 🇫🇷 Français</a>
-   <a href="./README_en.md"> 🇬🇧 English</a>
