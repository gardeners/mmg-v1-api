<?php

namespace Database\Seeders;

use App\Models\OpenWeatherMapCode;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OpenWeatherMapCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $path = __DIR__ . "/data/openweathermapcodes.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData['fr']['codes'] as $record) {
            OpenWeatherMapCode::firstOrCreate(
                [
                    "id" => $record['code'],
                    "name" => $record['name'],
                    "description" => $record['description'],
                ]
            );
        }
    }
}
