<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TaskDefinitionType;

class TaskDefinitionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $path = __DIR__ . "/data/task-definition-types.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData['tasks'] as $record) {
            TaskDefinitionType::firstOrCreate([
                "name" => $record['name']
            ]);

        }
    }
}
