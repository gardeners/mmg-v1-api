<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


use App\Models\User;
use App\Models\Team;
use App\Models\Zone;
use App\Models\Plant;
use App\Models\Task;
use Backpack\PermissionManager\app\Models\Role;

class GardenerDocusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $model = [
            "name" => "docus",
            "email" => 'docus@pangaia.app'
        ];
        $user = User::where($model)->first();
        if (empty($user)) {
            $model["password"] = Hash::make('123Soleil123Soleil');
            $user = User::create($model);
        }
        $user->assignRole(Role::findByName(User::ROLE_USER, 'web'));
        $user->assignRole(Role::findByName(User::ROLE_BOTANIST, 'web'));
        $user->assignRole(Role::findByName(User::ROLE_ADMIN, 'web'));

        // Create the garden
        $user->ownedTeams()->save(
            Team::unguarded(function () use ($user) {
                return Team::firstOrCreate([
                    'user_id' => $user->id,
                    'name' => "Jardin de " . explode(' ', $user->name, 2)[0],
                    'personal_team' => true
                ]);
            })
        );
        // Create the zones
        $zone = Zone::firstOrCreate([
            "garden_id" => $user->currentTeam()->first()->id,
            "name" => "Docus' veggies",
            "width" => 50,
            "height" => 100,
            "position_top" => 150,
            "position_left" => 250

        ]);
        $verger = Zone::firstOrCreate([
            "garden_id" => $user->currentTeam()->first()->id,
            "name" => "Docus' fruits",
            "width" => 100,
            "height" => 50,
            "position_top" => 250,
            "position_left" => 150

        ]);


        $tomato = Plant::firstOrCreate([
            "zone_id" => $zone->id,
            "name" => "Tomate green zebra"
        ]);
        $peach = Plant::firstOrCreate([
            "zone_id" => $verger->id,
            "name" => "Pêcher grosse mignone"
        ]);
        $strawberry = Plant::firstOrCreate([
            "zone_id" => $verger->id,
            "name" => "Fraise mara des bois"
        ]);

        foreach ([$strawberry, $tomato, $peach] as $plant) {

            Task::firstOrCreate([
                "plant_id" => $plant->id,
                "name" => "Arroser les {$plant->name}",
                "description" => "L'eau ça mouille les {$plant->name}"
            ]);
        }
    }
}