<?php

namespace Database\Seeders;

use App\Models\Difficulty;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DifficultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $path = __DIR__ . "/data/difficulties.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData as $key => $record) {
            Difficulty::firstOrCreate([
                'name' => $record['name'],
                'level' => $record['level'] ?? 1
            ]);
        }
    }
}
