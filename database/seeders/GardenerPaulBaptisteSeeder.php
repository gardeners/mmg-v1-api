<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Team;
use App\Models\Zone;
use App\Models\Plant;
use App\Models\Task;
use Backpack\PermissionManager\app\Models\Role;

class GardenerPaulBaptisteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $model = [
            "name" => "paul",
            "email" => 'paul@pangaia.app'
        ];
        $paul = User::where($model)->first();
        if (empty($paul)) {
            $model["password"] = Hash::make('Passwordpassword42');
            $paul = User::create($model);
        }
        $paul->assignRole(Role::findByName(User::ROLE_USER, 'web'));

        // Create the garden
        $paul->ownedTeams()->save(
            Team::unguarded(function () use ($paul) {
                return Team::firstOrCreate([
                    'user_id' => $paul->id,
                    'name' => "Jardin de " . explode(' ', $paul->name, 2)[0],
                    'personal_team' => true
                ]);
            })
        );

        // Create the user
        $model = [
            "name" => "baptiste",
            "email" => 'baptiste@pangaia.app'
        ];
        $bapt = User::where($model)->first();
        if (empty($bapt)) {
            $model["password"] = Hash::make('Passwordpassword42');
            $bapt = User::create($model);
        }
        $bapt->assignRole(Role::findByName(User::ROLE_USER, 'web'));


        // Create the garden

        $bapt->ownedTeams()->save(
            Team::unguarded(function () use ($bapt) {
                return Team::firstOrCreate([
                    'user_id' => $bapt->id,
                    'name' => "Jardin de " . explode(' ', $bapt->name, 2)[0],
                    'personal_team' => true
                ]);
            })
        );
        // Create a shared garden
        $common_garden = Team::unguarded(function () use ($paul) {
            return Team::firstOrCreate([
                'user_id' => $paul->id,
                'name' => "Bapt & Paul's Garden",
                'personal_team' => false
            ]);
        });

        $paul->ownedTeams()->save($common_garden);
        $bapt->teams()->save($common_garden);

        // Create the zones
        $zone = Zone::firstOrCreate([
            "garden_id" => $common_garden->id,
            "name" => "Potager partagé",
            "width" => 100,
            "height" => 100,
            "position_top" => 0,
            "position_left" => 0

        ]);
        $verger = Zone::firstOrCreate([
            "garden_id" => $common_garden->id,
            "name" => "Verger partagé",
            "width" => 100,
            "height" => 100,
            "position_top" => 0,
            "position_left" => 120

        ]);

        $zone3 = Zone::firstOrCreate([
            "garden_id" => $common_garden->id,
            "name" => "Zone 3",
            "width" => 100,
            "height" => 100,
            "position_top" => 0,
            "position_left" => 230
        ]);

        $zone4 = Zone::firstOrCreate([
            "garden_id" => $common_garden->id,
            "name" => "Zone 4",
            "width" => 100,
            "height" => 100,
            "position_top" => 0,
            "position_left" => 340
        ]);

        $zone5 = Zone::firstOrCreate([
            "garden_id" => $common_garden->id,
            "name" => "Zone 5",
            "width" => 215,
            "height" => 100,
            "position_top" => 110,
            "position_left" => 0
        ]);

        $zone6 = Zone::firstOrCreate([
            "garden_id" => $common_garden->id,
            "name" => "Zone 6",
            "width" => 215,
            "height" => 100,
            "position_top" => 110,
            "position_left" => 225
        ]);

        $tomato = Plant::firstOrCreate([
            "zone_id" => $zone->id,
            "name" => "Tomate Coeur de boeuf"
        ]);
        $cornichons = Plant::firstOrCreate([
            "zone_id" => $verger->id,
            "name" => "Cornichons"
        ]);

        foreach ([$cornichons, $tomato] as $plant) {
            Task::firstOrCreate([
                "plant_id" => $plant->id,
                "name" => "Arroser les {$plant->name}",
                "description" => "L'eau ça mouille les {$plant->name}"
            ]);
        }
    }
}
