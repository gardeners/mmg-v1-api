<?php

namespace Database\Seeders;

use App\Models\Hs\HsLevel;
use App\Models\Hs\HsProfile;
use App\Models\User;
use Illuminate\Database\Seeder;

class HsProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $docus = User::where("name", "Docus")->first();
        $level = HsLevel::where('name', 'Lombric')->first();
        HsProfile::firstOrCreate([
            "name" => "Robin",
            "user_id" => $docus->id,
            "profile_picture" => "/hs/avatar/sketch-beaver.png",
            "level" => $level->id
        ]);
        HsProfile::firstOrCreate([
            "name" => "Jeanne",
            "user_id" => $docus->id,
            "profile_picture" => "/hs/avatar/sketch-beaver.png",
            "level" => $level->id
        ]);

    }
}
