<?php

namespace Database\Seeders;

use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsPlantDefinitionIndication;
use App\Models\PlantDefinition;
use Illuminate\Database\Seeder;

class HsPlantDefinitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $path = __DIR__ . "/data/hs/hs-data.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData as $record) {
            $hs = HsPlantDefinition::firstOrCreate([
                "level" => $record['level'],
                "plant_definitions_id" => PlantDefinition::where('latin_name', $record['latin_name'])->first()->id
            ]);

            foreach ($record["before"] as $node) {
                HsPlantDefinitionIndication::firstOrCreate([
                    "message" => $node["text"],
                    "step" => "before",
                    "hs_plant_definition_id" => $hs->id
                ]);
            }

            foreach ($record["after"] as $node) {
                HsPlantDefinitionIndication::firstOrCreate([
                    "message" => $node["text"],
                    "step" => "after",
                    "hs_plant_definition_id" => $hs->id
                ]);
            }
        }
    }
}
