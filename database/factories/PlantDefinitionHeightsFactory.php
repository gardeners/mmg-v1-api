<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlantDefinitionLenghts>
 */
class PlantDefinitionHeightsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'min_height' => fake()->numberBetween(10, 1000),
            'max_height' => fake()->numberBetween(10, 1000),
        ];
    }
}
