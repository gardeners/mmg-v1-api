<?php

namespace Database\Factories;

use App\Models\Plant;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\TaskDefinition;
use App\Models\Zone;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlantFamily>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->sentence(),
            'task_definition_id' => TaskDefinition::factory()->create()->id,
            'plant_id' => Plant::factory()->create()->id,
            'description' => fake()->paragraph(),
            'date_beg' => fake()->dateTimeInInterval('-1 week', '0 days')->format('Y-m-d'),
            'date_end' => fake()->dateTimeInInterval('1 days', '1 week')->format('Y-m-d'),
        ];
    }
}
