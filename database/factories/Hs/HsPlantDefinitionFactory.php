<?php

namespace Database\Factories\Hs;

use App\Models\Hs\HsLevel;
use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsPlantDefinitionIndication;
use App\Models\PlantDefinition;
use Illuminate\Database\Eloquent\Factories\Factory;

class HsPlantDefinitionFactory extends Factory
{
    protected $model = HsPlantDefinition::class;

    public function definition(): array
    {
        return [
            'level' => HsLevel::factory()->create()->id,
            'plant_definitions_id' => PlantDefinition::factory()->create()->id,
        ];
    }

    public function hasHsPlantDefinitionIndications(int $count = 1)
    {
        return $this->has(HsPlantDefinitionIndication::factory()->count($count), 'hs_plant_definition_indication');
    }
}
