<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlantDefinitionRequirements>
 */
class PlantDefinitionRequirementsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'exposition_label' => $this->faker->randomElement(['Faible', 'Moyen', 'Fort']),
            'humidity_label' => $this->faker->randomElement(['Faible', 'Moyen', 'Fort']),
            'exposition_level' => $this->faker->randomDigit(1, 3),
            'humidity_level' => $this->faker->randomDigit(1, 3),
        ];
    }
}
