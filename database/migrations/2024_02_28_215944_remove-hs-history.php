<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
{
    Schema::table('hs_profile_historys', function ($table) {
        $table->dropForeign(['hs_history_id']);
    });

    Schema::dropIfExists('hs_historys');
}

public function down(): void
{
    Schema::create('hs_historys', function ($table) {
        $table->id();
    });

    Schema::table('hs_profile_historys', function ($table) {
        $table->foreign('hs_history_id')->references('id')->on('hs_historys');
    });
}
};
