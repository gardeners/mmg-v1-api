<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up()
    {
        DB::unprepared("DROP PROCEDURE IF EXISTS DeleteOldDemoAccounts");
        DB::unprepared("
        CREATE PROCEDURE DeleteOldDemoAccounts()
        BEGIN
          DELETE FROM users
          WHERE email LIKE '%@demo.pangaia.app'
            AND created_at <= NOW() - INTERVAL 1 DAY;
        END
    ");

        DB::unprepared("
        CREATE EVENT IF NOT EXISTS event_DeleteOldDemoAccounts
        ON SCHEDULE EVERY 1 DAY
        DO
          CALL DeleteOldDemoAccounts();
    ");
    }

    public function down()
    {
        DB::unprepared("DROP EVENT IF EXISTS event_DeleteOldDemoAccounts");
        DB::unprepared("DROP PROCEDURE IF EXISTS DeleteOldDemoAccounts");
    }
};


